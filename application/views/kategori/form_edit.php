<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>
         
          <div class="row">
            <div class="col-lg">
<h3>Edit Kategori</h3>
<?php
    echo form_open('kategori/edit');
?>
 <input  type="hidden" name="id" value="<?php echo $record['kategori_id']?>">
<table class="table table-bordered">
    <tr>
        <td width="130">Nama Kategori</td>
        <td><div class="col-sm-4"><input class="form-control" type="text" name="kategori" placeholder="kategori" 
                   value="<?php echo $record['nama_kategori'];?>"></div></td>
    </tr>
    <tr>
         <td colspan="2"><button type="submit" class="btn btn-primary btn-sm" name="submit">Simpan</button>
        <?php echo anchor('kategori','Kembali',array('class'=>'btn btn-primary btn-sm'))?></td>
    </tr>
</table>
</form>