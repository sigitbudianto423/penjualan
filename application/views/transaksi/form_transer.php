<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>
         
          <div class="row">
            <div class="col-lg">
        
<?php
    echo form_open(); 
?>
<?= $this->session->flashdata('message');  ?>
<table class="table table-bordered">
<tr>
        <td>
            <div class="">
            <label for="">Kode Barang</label>
                    <input list="barang" id="kode_barang" name="kode_barang" placeholder="masukan nama kode" class="form-control">
                    <input type="hidden" name="barang_id"  placeholder="id" id="barang_id" required>
            </div>
            <hr>
            <div class="">
            <label for="">Nama Barang</label>
                    <input  id="nama_barang" name="nama_barang"  class="form-control" readonly>
                   
            </div>
            <hr>
            <div class="">
            <label for="">Harga</label>
                    <input  id="harga" name="harga"  class="form-control" readonly>
                   
            </div>
            <hr>
            <div class="">
            <label for="">QTY</label>
                    <input type="text" name="qty" placeholder="QTY" class="form-control" required>
                   
                    </div>
        </td>
    </tr>
    <tr>
        <td>
            
            <button type="submit" name="submit" class="btn btn-primary">Simpan</button>
           
        </td>
    </tr>

</table>
</form>
<table class="table table-bordered">
    <tr class="success"><th colspan="6">Detail Transaksi</th></tr>
    <tr><th>No</th><th>Nama Barang</th><th>QTY</th><th>Harga</th><th>Subtotal</th><th>Cencel</th></tr>
    <?php 
    $no=1;
    $total=0;
        foreach ($detail as $r)
            {
                echo "<tr><td>$no</td>
                    <td>$r->nama_barang</td>
                    <td>$r->qty</td>
                    <td>$r->harga</td>
                    <td>".$r->qty*$r->harga."</td>
                    <td>".anchor('transer/hapusitem/'.$r->t_detail_id,'Hapus',array('class'=>'btn btn-danger'))." </td>
                        </tr>";
                $total=$total+($r->qty*$r->harga);
                $no++;
            }
    ?>
    <tr><td colspan="5"><p align="right">Total</p></td><td><?php echo $total; ?></td></tr>
</table>

<datalist id="barang">
    <?php 
        foreach ($barang->result() as $b)
        {
            echo "<option value='$b->nama_barang'></option>";
        }
    ?>
</datalist>

<table align ="right">
        <tr>
            <td>
            <?php echo anchor('transer/selesai_belanja','selesai',array('class'=>'btn btn-success'));?>
            <?php echo anchor('transer/nota','cetak',array('class'=>'btn btn-info'));?>

            </td>
        </tr>
</table>
 </div>
    
        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <script type="text/javascript">
        $(document).ready(function(){
            $( "#title" ).autocomplete({
              source: "<?php echo site_url('transer/get_autocomplete/?');?>"
            });
        });
    </script>
    <script src="<?php echo base_url();?>assets/js/jquery.min.js"></script>
                <script type="text/javascript">
$(function() {
        $("#kode_barang").on("change",function(){
            var id_pemesan = $("#kode_barang").val();

            $.ajax({
                url: '<?php echo site_url('transaksi/autofill'); ?>',
                type: 'POST',
                dataType: 'json',
                data: {
                    'kode_barang': id_pemesan
                },
                success: function (pemesan) {
                    $("#nama_barang").val(pemesan.nama_barang);
                    $("#harga").val(pemesan.harga);
                    $("#barang_id").val(pemesan.barang_id);
                   
                }
            });
        });
    });
      </script>
 