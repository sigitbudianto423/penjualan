
<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>
         
          <div class="row">
            <div class="col-lg">
<h3>Laporan Transaksi</h3>
<?php 
echo form_open('transaksi/laporan');
?>
<table class="table table-bordered">
    <tr>
        <td>
            <div class="col-sm-2">
            <input class="form-control" type="date" name="tanggal1" placeholder="tanggal1">
            </div>
            <hr>
            <div class="col-sm-2">
            <input class="form-control" type="date" name="tanggal2" placeholder="tanggal2">
            </div>
       
        </td>
         
    </tr>
    <tr>
        <td><button class="btn btn-primary btn-sm" name="submit" type="submit">Proses</button></td>
    </tr>
</table>
</form>
<hr>
<table id="table" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
    <tr>
        <th>No</th>
        <th>Tanggal Transaksi</th>
        <th>Operator</th>
        <th>Total Transaksi</th>
        <th>opsi</th>
    </tr>
    </thead>
    <tbody id="mytab">
    <?php
    $no=1;
    $total=0;
    foreach ($record as $r)
    {
        ?>
        <tr>
            <td width='10'><?php echo $no ?></td>
            <td width='160'><?php echo $r->tanggal_transaksi ?></td>
            <td><?php echo $r->name ?></td>
            <td><?php echo $r->total?></td>
            <td>
                <?= anchor('transaksi/hapus/'.$r->transaksi_id,'Hapus',array('class'=>'btn btn-danger')) ?>
                <a href="" class="btn btn-success detail" data-toggle="modal" data-target="#Modal" data-id="<?= $r->transaksi_id; ?>">Detail</a>
                           </td>
        </tr>
        
        <?php
        $no++;
        $total=$total+$r->total;
    }
    ?>
   
         <tr>
            <td colspan='4'>Total</td><td><?php echo $total?></td>
        </tr>
         </tbody>
</table>

<div class="modal fade" id="Modal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="ModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">      
       <hr>
            <table class="table table-striped table-bordered" width="100%" cellspacing="0">
                <thead>
                      
                  <tr>
                      <th>Kode Barang</th>
                      <th>Nama Barang</th>
                      <th>QTY</th>
                      <th>Harga</th>
                  </tr>
                </thead>
                <tbody id="show_data">
                  
                </tbody>
            </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        
      </div>
    </div>
  </div>
</div>

