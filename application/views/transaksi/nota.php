<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>
         
          <div class="row">
            <div class="col-lg">
<h3>Edit Kategori</h3>
<?php
    echo form_open('transaksi/detail');
?>
<input type="hidden" name="id" value="<?php echo $record['transaksi_id']?>">
<table class="table table-bordered">
<tr>
        <td width="130">kode Barang</td>
        <td><input class="form-control" type="text" name="kodebarang"  value="<?php echo $record['tanggal_transaksi'] ?>" placeholder="nama barang" readonly></td>
    </tr>
    <tr>
        <td width="130">Nama Barang</td>
        <td><input class="form-control" type="text" name="nama_barang"  value="<?php echo $record['nama_barang'] ?>" placeholder="nama barang"></td>
    </tr>
    
    <tr>
        <td width="130">Kategori</td>
        <td> <select name="kategori" class="form-control">
        <?php
                foreach ($kategori as $k)
                {
                    echo "<option value='$k->kategori_id'";
                    echo $record['kategori_id']==$k->kategori_id?'selected':'';
                    echo">$k->nama_kategori</option>";
                }
        ?>
    </select></td>
    </tr>
    <tr>
        <td width="130">Harga</td>
        <td><input class="form-control" type="text" name="harga" value="<?php echo $record['harga']?>" placeholder="harga"></td>
    </tr>
    
    <tr>
         <td colspan="2"><button type="submit" class="btn btn-primary btn-sm" name="submit">Simpan</button>
        <?php echo anchor('barang','Kembali',array('class'=>'btn btn-primary btn-sm'))?></td>
    </tr>
</table>
</form>