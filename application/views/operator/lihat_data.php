
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Book Display</title>
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.12/css/dataTables.bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css"/>
    </head>
<h3>Data Operator</h3>
<?php
echo anchor('operator/post','Tambah Data',array('class'=>'btn btn-success'));
?>
<hr>
<table id="table" class="table table-striped table-bordered" width="100%" cellspacing="0">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama lengkap</th>
            <th>User Name</th>
            <th>Login Terakhir</th>
            <th>Operasi</th>
        </tr>
    </thead>
    <tbody>
         <?php
        $no=1;
        foreach ($record->result() as $r) {
             echo "<tr>
                        <td width='10'>$no</td>
                        <td >$r->nama_lengkap </td>
                        <td >$r->username </td>
                        <td>$r->last_login </td>
                        <td>".  anchor('operator/edit/'.$r->operator_id,'Edit',array('class'=>'btn btn-success'))."
                            ".  anchor('operator/delete/'.$r->operator_id,'Delete',array('class'=>'btn btn-success'))."</td>
                        
                  </tr>"; 
             $no++;
        }
    ?>
    </tbody>
</table>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> 
<script type="text/javascript">
$(document).ready( function () {
    $('#table').DataTable();
} );
</script>