<h3>Tambah operator</h3>
<?php
    echo form_open('operator/post');
?>
<table class="table table-bordered">
    <tr>
        <td width="130">nama lengkap</td>
        <td><input class="form-control" type="text" name="nama" placeholder="nama lengkap"></td>
    </tr>
    <tr>
        <td width="130">Username</td>
        <td><input class="form-control" type="text" name="username" placeholder="username"></td>
    </tr>
    <tr>
        <td width="130">password</td>
        <td><input class="form-control" type="password" name="password" placeholder="password"></td>
    </tr>
    <tr>
        <td colspan="2"><button type="submit" class="btn btn-primary btn-sm" name="submit">Simpan</button>
        <?php echo anchor('operator','Kembali',array('class'=>'btn btn-primary btn-sm'))?></td>
    </tr>
</table>
</form>