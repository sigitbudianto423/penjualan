<h3>Edit Data Operator</h3>
<?php
    echo form_open('operator/edit');
?>
<input type="hidden" name="id" value="<?php echo $record['operator_id']?>">
<table class="table table-bordered">
    
    <tr>
    
        <td width="130">Nama lengkap</td>
        <td><input class="form-control" type="text" name="nama" placeholder="nama lengkap" 
                   value="<?php echo $record['nama_lengkap'];?>"></td>
    </tr>
    
    <tr>
         <td>Username</td>
        <td><input class="form-control" type="text" name="username" placeholder="username" 
                   value="<?php echo $record['username'];?>"></td>
    </tr>
    <tr>
         <td>Password</td>
        <td><input class="form-control" type="password" name="password" placeholder="password" ></td>
    </tr>
    <tr>
        <td colspan="2"><button type="submit" class="btn btn-primary btn-sm" name="submit">Simpan</button>
        <?php echo anchor('operator','Kembali',array('class'=>'btn btn-primary btn-sm'))?></td>
    </tr>
</table>
</form>