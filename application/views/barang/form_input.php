<div class="container-fluid">

          <!-- Page Heading -->
          <h1 class="h3 mb-4 text-gray-800"></h1>
         
          <div class="row">
            <div class="col-lg">
<h1>Tambah Buku</h1>
<?php
    echo form_open('transer/post');
?>

<?= $this->session->flashdata('message');  ?>
<table class="table table-striped">
    <tr>
        <td width="130">Nama Barang</td>
        <td><div class="col-sm-4"><input class="form-control" type="text" name="nama_barang" placeholder="nama barang" required></div></td>
    </tr>
    
    <tr>
        <td width="130">Kategori</td>
        <td> <div class="col-sm-4"> <select name="kategori" class="form-control" >
        <?php
                foreach ($kategori as $k)
                {
                    echo "<option value='$k->kategori_id'>$k->nama_kategori</option>";
                }
        ?>
    </select></div></td>
    </tr>
    <tr>
        <td width="130">Harga</td>
        <td><div class="col-sm-4" ><input class="form-control" type="text" name="harga" placeholder="harga" required></div></td>
    </tr>
    
    <tr>
         <td colspan="2"><button type="submit" class="btn btn-primary btn-sm" name="submit">Simpan</button>
        <?php echo anchor('transer','Kembali',array('class'=>'btn btn-primary btn-sm'))?></td>
    </tr>
</table>
</form>