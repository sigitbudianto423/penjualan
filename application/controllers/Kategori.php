<?php
class Kategori extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model('model_kategori');
    }


    function index (){
        $data['record']= $this->model_kategori->tampilkan_data();
        //$this->load->view('kategori/lihat_data',$data);
         $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();
            $data['title'] = 'Kategori';
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('kategori/lihat_data',$data);
        $this->load->view('templates/footer');
        

    }
    
    function post(){
        
        if(isset($_POST['submit'])){
            $this->model_kategori->post();
            redirect('kategori');
        }
        else {
           // $this->load->view('kategori/form_input');
              $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();
            $data['title'] = 'Tambah Kategori';
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('kategori/form_input');
        $this->load->view('templates/footer');
        
        }
    }
    function edit(){
        if(isset($_POST['submit'])){
            $this->model_kategori->edit();
            redirect('kategori');
        }
        else {
            $id = $this->uri->segment(3);
            $data['record'] = $this->model_kategori->get_one($id)->row_array();
            //$this->load->view('kategori/form_edit',$data); 
           $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();
            $data['title'] = 'Tambah Kategori';
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('kategori/form_edit',$data);
        $this->load->view('templates/footer');
        }
    }
    
    function delete(){
        $id = $this->uri->segment(3);
        $this->model_kategori->delete($id);
        redirect('kategori');
    }
}
 