<?php
class Transaksi extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model(array('model_barang','model_transaksi'));
        
    } 


    function index()
    { 
        $this->form_validation->set_rules('barang', 'Barang', 'required|trim');
        $this->form_validation->set_rules('qty', 'Qty', 'required|trim');
       if (isset($_POST['submit']))
        {
            $nama = $this->input->post('kode_barang');
            $ba = $this->db->get_where('barang', ['kode_barang'=> $nama])->row_array();
            if($ba == true){
                $this->model_transaksi->simpan_barang();
                redirect('transaksi');
            }else{
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
												barang tidak ada! dan Qty harus di isi
	  											</div>');
                                                  redirect('transaksi'); 
            }
              
       }else{
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();
            $data['title'] = 'From Transaksi';
            $data['barang'] = $this->model_barang->tampilkan_data();
            $data['detail'] = $this->model_transaksi->tampilkan_detail_transaksi()->result();
            $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebar',$data);
            $this->load->view('templates/topbar',$data);
            $this->load->view('transaksi/form_transaksi',$data);
            $this->load->view('templates/footer');
       }
    }

    function hapus()
    {
        $id = $this->uri->segment(3);
        $this->model_transaksi->hapustransaksi($id);
        redirect('transaksi/laporan');
    }
    
    function hapusitem()
    {
        $id = $this->uri->segment(3);
        $this->model_transaksi->hapusitem($id);
        redirect('transaksi');
    }
    
    function selesai_belanja()
    {
        $tanggal =  date('y-m-d');
        $user = $this->session->userdata('email');
        $id_op = $this->db->get_where('user',array('email'=>$user))->row_array();
        $data = array('oprator_id'=>$id_op['oprator_id'],
        'tanggal_transaksi'=>$tanggal);
        $this->model_transaksi->selesai_belanja($data);
        redirect('transaksi');
    }
    
    function laporan()
    {
        if (isset($_POST['submit'])){

            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();
            $data['title'] = 'Laporan Transaksi';
            $tanggal1 = $this->input->post('tanggal1');
            $tanggal2 = $this->input->post('tanggal2');
            $data['record']= $this->model_transaksi->laporan_periode($tanggal1,$tanggal2);
            $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebar',$data);
            $this->load->view('templates/topbar',$data);
            $this->load->view('transaksi/laporan',$data);
            $this->load->view('templates/footer');
       
        }  else {
             $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();
            $data['title'] = 'From Transaksi';
            $data['record']= $this->model_transaksi->laporan_default()->result();
             $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebar',$data);
            $this->load->view('templates/topbar',$data);
            $this->load->view('transaksi/laporan',$data);
            $this->load->view('templates/footer');
        
    }
}
    
    function excel()
    {
        header("Content-type: application/x-ms-excel");
        header("Content-Disposition: attachment; filename=laporantransaksi.xls");
        $data['record']= $this->model_transaksi->laporan_default();
        $this->load->view('transaksi/laporan_excel',$data); 
    }
    
    function pdf()
    {
        $this->load->library('pdf');
        $pdf = new FPDF('P','mm','A4');
        $pdf->AddPage();
        $pdf->SetFont('Arial','B','L');
        $pdf->SetFontSize(14);
        $pdf->Text(10,10 ,'Laporan Transaksi');
        $pdf->SetFont('Arial','B','L');
        $pdf->SetFontSize(10);
        $pdf->Cell(10,10,'','',1);
        $pdf->Cell(10, 7, 'No', 1, 0);
        $pdf->Cell(27, 7, 'Tanggal', 1, 0);
        $pdf->Cell(30, 7, 'operator', 1, 0);
        $pdf->Cell(36, 7, 'Total Transaksi', 1, 1);
        $pdf->SetFont('Arial','','L');
        $data =  $this->model_transaksi->laporan_default();
        $no=1;
        $total=0;
        foreach ($data->result()as $d)
        {
            $pdf->Cell(10, 7, $no, 1, 0);
            $pdf->Cell(27, 7, $d->tanggal_transaksi, 1, 0);
            $pdf->Cell(30, 7, $d->name, 1, 0);
            $pdf->Cell(36, 7, $d->total, 1, 1);
            $no++;
            $total=$total+$d->total;
        }
        $pdf->Cell(67,7,'Total',1,0,'R');
        $pdf->Cell(36,7,$total,1,0);
        $pdf->Output();
    }
    
    
    
    function cetak()
    {
            $id = $this->uri->segment(3);
//            $data['record']= $this->model_transaksi->get_one($id);
            $data['barang']= $this->model_transaksi->cetak($id)->result();
            $this->template->load('template','transaksi/nota',$data);
        }

        
        
        function nota()
        {
        $this->load->library('pdf');
        $pdf = new FPDF('P','mm','A4');
        $pdf->AddPage();
        $pdf->SetFont('Arial','B','L');
        $pdf->SetFontSize(14);
        $pdf->Text(10,10 ,'Jumlah Belanja');
        $pdf->SetFont('Arial','B','L');
        $pdf->SetFontSize(10);
        $pdf->Cell(10,10,'','',1);
        $pdf->Cell(10, 7, 'No', 1, 0);
        $pdf->Cell(30, 7, 'Nama Barang', 1, 0);
        $pdf->Cell(30, 7, 'QTY', 1, 0);
        $pdf->Cell(36, 7, 'harga', 1, 0);
        $pdf->Cell(36, 7, 'jumlah', 1, 1);
        $pdf->SetFont('Arial','','L');
        $data =  $this->model_transaksi->tampilkan_detail_transaksi();
        $no=1;
        $total=0;
        foreach ($data->result()as $d)
        {
            $pdf->Cell(10, 7, $no, 1, 0);
            $pdf->Cell(30, 7, $d->nama_barang, 1, 0);
            $pdf->Cell(30, 7, $d->qty, 1, 0);
            $pdf->Cell(36, 7, $d->harga, 1, 0);
             $pdf->Cell(36, 7, $d->harga*$d->qty, 1, 1);
            $no++;
            $total=$total+$d->harga*$d->qty;
        }
        $pdf->Cell(106,7,'Total',1,0,'R');
        $pdf->Cell(36,7,$total,1,0);
        $pdf->Output();
    
        }

        function get_autocomplete(){
            if (isset($_GET['term'])) {
                $result = $this->model_transaksi->search_blog($_GET['term']);
                if (count($result) > 0) {
                    foreach ($result as $row)
                        $arr_result[] = array(
                            'kode_barang'    => $row->kode_barang,
                     );
                        echo json_encode($arr_result);
                }
            }
        }

        function autofill(){
            $nis=$this->input->post('kode_barang');
            $data=$this->model_transaksi->get_nis($nis);
            echo json_encode($data);
        }

        function detail(){
            $data = $this->model_transaksi->cetak($_POST['id'])->result();
          echo json_encode($data);
          
        }

       


}

 
    
   
