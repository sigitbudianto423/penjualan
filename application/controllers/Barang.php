<?php
class Barang extends CI_Controller
{
    
    function __construct() {
        parent::__construct();
        $this->load->model(array('model_barang','model_transaksi','model_kategori'));
    }


    function index()
        {
            $data['title'] = 'Semua Daftar Buku';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
            $data['record'] = $this->model_barang->tampilkan_data()->result();
            $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebar',$data);
            $this->load->view('templates/topbar',$data);
            $this->load->view('barangad/lihat_data',$data);
            $this->load->view('templates/footer');
        }

    
     function fiksi()
        {
           $data['title'] = 'Daftar Buku Fiksi';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
        $data['record'] = $this->model_barang->fiksi();
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('barangad/fiksi',$data);
        $this->load->view('templates/footer');
        }

    function nonfiksi()
        {
            $data['title'] = 'Daftar Buku Non Fiksi';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
        $data['record'] = $this->model_barang->nonfiksi();
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('barangad/nonfiksi',$data);
        $this->load->view('templates/footer');       
        }

    function post()
    {
        $dariDB = $this->model_barang->cekkodebarang();
        // contoh JRD0004, angka 3 adalah awal pengambilan angka, dan 4 jumlah angka yang diambil
        $nourut = substr($dariDB, 3, 4);
        $kodeBarangSekarang = $nourut + 1;
        $data = array('kode_barang' => $kodeBarangSekarang);
        
        if(isset($_POST['submit'])){
       
           $kode= $this->input->post('kodebarang');
           $nama        =   $this->input->post('nama_barang');
           $kategori    =   $this->input->post('kategori');
           $harga       =   $this->input->post('harga');

           $ba = $this->db->get_where('barang', ['nama_barang'=> $nama])->row_array();
           if($ba>0){
            echo "<script>window.alert('barang yang anda masukan sudah ada')
            window.location='post'</script>";
           }else{
                                                 $data       = array('nama_barang'=>$nama,
                                                 'kategori_id'=>$kategori,
                                                 'harga'=>$harga,
                                                 'kode_barang' => $kode
                                                );
                             $this->model_barang->post($data);
                             redirect('barang');
                                                }
       

        }
        else {
            $this->load->model('model_kategori');
            $data['kategori']=  $this->model_kategori->tampilkan_data();
            $data['title'] = 'Tambah Buku';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
        $data['record'] = $this->model_barang->nonfiksi();
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebar',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('barangad/form_input',$data);
        $this->load->view('templates/footer');       
        }
    }
    
    function edit()
    {
        if(isset($_POST['submit'])){
            
           $id          =   $this->input->post('id');
           $nama        =   $this->input->post('nama_barang');
           $kategori    =   $this->input->post('kategori');
           $harga       =   $this->input->post('harga');
           $data        = array('nama_barang'=>$nama,
                                'kategori_id'=>$kategori,
                                'harga'=>$harga);
           $this->model_barang->edit($data,$id);
           redirect('barang/buku');
        }
        else {
            $id = $this->uri->segment(3);
            $data['kategori'] =  $this->model_kategori->tampilkan_data();
            $data['record'] = $this->model_transaksi->get_one($id)->row_array();
            $data['title'] = 'edit Buku';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
            $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebar');
            $this->load->view('templates/topbar',$data);
            $this->load->view('barangad/form_edit',$data);
            $this->load->view('templates/footer'); 
        }
    }
    
    function delete()
    {
       $id = $this->uri->segment(3);
       $this->model_barang->delete($id);
       redirect('barang');
     }
   }
