<?php
class Transer extends CI_Controller
{
    function __construct() {
        parent::__construct();
        $this->load->model(array('model_barang','model_transaksi','model_kategori'));
    }


   public function index()
    {
        $this->form_validation->set_rules('barang', 'Barang', 'required|trim');
        $this->form_validation->set_rules('qty', 'Qty', 'required|trim');
       if (isset($_POST['submit']))
        {
            $nama = $this->input->post('kode_barang');
        $ba = $this->db->get_where('barang', ['kode_barang'=> $nama])->row_array();
            if($ba == true){
                $this->model_transaksi->simpan_barang();
                redirect('transer');
            }else{
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
												barang tidak ada! dan Qty harus di isi
	  											</div>');
                                                  redirect('transer'); 
            }
              
       }else{
             $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();
            $data['title'] = 'From Transaksi';
            $data['barang'] = $this->model_barang->tampilkan_data();
            $data['detail'] = $this->model_transaksi->tampilkan_detail_transaksi()->result();
            $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebarus',$data);
            $this->load->view('templates/topbar',$data);
            $this->load->view('transaksi/form_transer',$data);
            $this->load->view('templates/footer');
       }
    }

    function hapus()
    {
        $id = $this->uri->segment(3);
        $this->model_transaksi->hapustransaksi($id);
        redirect('transer/laporan');
    }
    
    function hapusitem()
    {
        $id = $this->uri->segment(3);
        $this->model_transaksi->hapusitem($id);
        redirect('transer');
    }
    
    function selesai_belanja()
    {
        $tanggal =  date('y-m-d');
        $user = $this->session->userdata('email');
        $id_op = $this->db->get_where('user',array('email'=>$user))->row_array();
        $data = array('oprator_id'=>$id_op['oprator_id'],'tanggal_transaksi'=>$tanggal);
        $this->model_transaksi->selesai_belanja($data);
        redirect('transer');
    }

  function buku()
        {
            $data['title'] = 'Semua Daftar Buku';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
            $data['record'] = $this->model_barang->tampilkan_data()->result();
            $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebarus',$data);
            $this->load->view('templates/topbar',$data);
            $this->load->view('barang/lihat_data',$data);
            $this->load->view('templates/footer');
        }

    
     function fiksi()
        {
           $data['title'] = 'Daftar Buku Fiksi';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
        $data['record'] = $this->model_barang->fiksi();
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebarus',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('barang/fiksi',$data);
        $this->load->view('templates/footer');
        }

    function nonfiksi()
        {
            $data['title'] = 'Daftar Buku Non Fiksi';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
        $data['record'] = $this->model_barang->nonfiksi();
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebarus',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('barang/nonfiksi',$data);
        $this->load->view('templates/footer');       
        }

    function post()
    {


        if(isset($_POST['submit'])){
           $nama        =   $this->input->post('nama_barang');
           $kategori    =   $this->input->post('kategori');
           $harga       =   $this->input->post('harga');
           $data        = array('nama_barang'=>$nama,
                                'kategori_id'=>$kategori,
                                'harga'=>$harga);
           $this->model_barang->post($data);
                      redirect('transer/buku');
        }
        else {
            $this->load->model('model_kategori');
            $data['kategori']=  $this->model_kategori->tampilkan_data();
            $data['title'] = 'Tambah Buku';
        $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
        $data['record'] = $this->model_barang->nonfiksi();
        $this->load->view('templates/header',$data);
        $this->load->view('templates/sidebarus',$data);
        $this->load->view('templates/topbar',$data);
        $this->load->view('barang/form_input',$data);
        $this->load->view('templates/footer');       
        }
    }
    
    function edit()
    {
        if(isset($_POST['submit'])){
            
           $id          =   $this->input->post('id');
           $nama        =   $this->input->post('nama_barang');
           $kategori    =   $this->input->post('kategori');
           $harga       =   $this->input->post('harga');
           $data        = array('nama_barang'=>$nama,
                                'kategori_id'=>$kategori,
                                'harga'=>$harga);
           $this->model_barang->edit($data,$id);
           redirect('transer/buku');
        }
        else {
            $id = $this->uri->segment(3);
            $data['kategori'] =  $this->model_kategori->tampilkan_data();
            $data['record'] = $this->model_barang->get_one($id)->row_array();
            $data['title'] = 'edit Buku';
            $data['user'] = $this->db->get_where('user',['email'=> $this->session->userdata('email')])->row_array();      
            $this->load->view('templates/header',$data);
            $this->load->view('templates/sidebarus');
            $this->load->view('templates/topbar',$data);
            $this->load->view('barang/form_edit',$data);
            $this->load->view('templates/footer'); 
        }
    }
    
    function delete()
    {
       $id = $this->uri->segment(3);
       $this->model_barang->delete($id);
       redirect('transer/buku');
    }

    function get_autocomplete(){
        if (isset($_GET['term'])) {
            $result = $this->model_transaksi->search_blog($_GET['term']);
            if (count($result) > 0) {
            foreach ($result as $row)
                $arr_result[] = $row->blog_title;
                echo json_encode($arr_result);
            }
        }
    }


    function cetak()
    {
            $id = $this->uri->segment(3);
//            $data['record']= $this->model_transaksi->get_one($id);
            $data['barang']= $this->model_transaksi->cetak($id)->result();
            $this->template->load('template','transaksi/nota',$data);
        }

        
        
        function nota()
        {
        $this->load->library('pdf');
        $pdf = new FPDF('P','mm','A4');
        $pdf->AddPage();
        $pdf->SetFont('Arial','B','L');
        $pdf->SetFontSize(14);
        $pdf->Text(10,10 ,'Jumlah Belanja');
        $pdf->SetFont('Arial','B','L');
        $pdf->SetFontSize(10);
        $pdf->Cell(10,10,'','',1);
        $pdf->Cell(10, 7, 'No', 1, 0);
        $pdf->Cell(30, 7, 'Nama Barang', 1, 0);
        $pdf->Cell(30, 7, 'QTY', 1, 0);
        $pdf->Cell(36, 7, 'harga', 1, 0);
        $pdf->Cell(36, 7, 'jumlah', 1, 1);
        $pdf->SetFont('Arial','','L');
        $data =  $this->model_transaksi->tampilkan_detail_transaksi();
        $no=1;
        $total=0;
        foreach ($data->result()as $d)
        {
            $pdf->Cell(10, 7, $no, 1, 0);
            $pdf->Cell(30, 7, $d->nama_barang, 1, 0);
            $pdf->Cell(30, 7, $d->qty, 1, 0);
            $pdf->Cell(36, 7, $d->harga, 1, 0);
             $pdf->Cell(36, 7, $d->harga*$d->qty, 1, 1);
            $no++;
            $total=$total+$d->harga*$d->qty;
        }
        $pdf->Cell(106,7,'Total',1,0,'R');
        $pdf->Cell(36,7,$total,1,0);
        $pdf->Output();
    
        }

    
    function search_blog($title){
        $this->db->like('kode_barang', $title , 'both');
        $this->db->order_by('kode_barang', 'ASC');
        $this->db->limit(10);
        return $this->db->get('barang')->result();
    }

    function get_nis($nis){
        $hsl=$this->db->query("SELECT * FROM barang WHERE kode_barang='$nis'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'kode_barang' => $data->kode_barang,
                    'nama_barang' => $data->nama_barang,
                    'harga' => $data->harga,
                    'barang_id' => $data->barang_id,
                    );
            }
        }
        return $hasil;
    }
}

 
    
   
