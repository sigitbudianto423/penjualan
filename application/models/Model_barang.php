<?php

class Model_barang extends CI_Model
{
    function tampilkan_data()
    {
        $query = "  SELECT b.barang_id ,b.kode_barang,b.nama_barang,b.harga,kb.nama_kategori 
                    from barang as b, kategori_barang as kb 
                    where b.kategori_id=kb.kategori_id";
      return  $this->db->query($query);
    }

    function fiksi(){
        $query = " SELECT * FROM barang INNER JOIN kategori_barang USING (kategori_id) WHERE kategori_id =1";
      return  $this->db->query($query)->result();
    }

    function nonfiksi(){
        $query = " SELECT * FROM barang INNER JOIN kategori_barang USING (kategori_id) WHERE kategori_id =2";
      return  $this->db->query($query)->result();
    }
    function post($data)    
    {
        $this->db->insert('barang',$data);
    }
    
    function get_one($id)
    {
        $param = array('barang_id'=>$id);
        return $this->db->get_where('barang',$param);
    }
    
    function edit($data,$id)
    {
        $this->db->where('barang_id',$id);
        $this->db->update('barang',$data);
    }
    
    function delete($id)
    {
        $this->db->where('barang_id',$id);
        $this->db->delete('barang');
    }

    public function val()
    {
        $nama = $this->input->post('nama_barang');
        $data = "SELECT * FROM barang WHERE nama='$nama' or email='$email'";
        return $this->db->query($data);
    }

    public function cekkodebarang()
    {
        $query = $this->db->query("SELECT MAX(kode_barang) as kodebarang from barang");
        $hasil = $query->row();
        return $hasil->kodebarang;
    }


}

