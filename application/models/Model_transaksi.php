<?php
class Model_transaksi extends CI_Model
{
    function simpan_barang()
    {
        $nama_barang    =    $this->input->post('kode_barang');
        $qty            =    $this->input->post('qty');
        $idbarang       = $this->db->get_where('barang',array('kode_barang'=>$nama_barang))->row_array();
        
        $data           = array('barang_id'=>$idbarang['barang_id'],
                                'qty'=>$qty,
                                'harga'=>$idbarang['harga']
                                ,'status'=>'0');
        $this->db->insert('transaksi_detail',$data);
    }
    
    function tampilkan_detail_transaksi()
    {
        $query= "SELECT  td.t_detail_id,td.qty,td.harga,b.nama_barang 
                    FROM transaksi_detail as td,barang as b 
                    WHERE b.barang_id=td.barang_id and status='0'";
        return $this->db->query($query);
    }
    
    function hapusitem($id)
    {
        $this->db->where('t_detail_id',$id);
        $this->db->delete('transaksi_detail');
    }

    function hapustransaksi($id)
    {
        $this->db->where('transaksi_id',$id);
        $this->db->delete(array('transaksi','transaksi_detail'));
    }
    
    function selesai_belanja($data)
    {
        $this->db->insert('transaksi',$data);
        $last_id = $this->db->query("select transaksi_id from transaksi order by transaksi_id desc")->row_array();
        $this->db->query("update transaksi_detail set transaksi_id='".$last_id['transaksi_id']."' where status='0'");
        $this->db->query("update transaksi_detail set status='1' where status='0'");
    }
    
    function laporan_default()
    {
            $query = "SELECT td.transaksi_id, t.tanggal_transaksi, o.name, sum(td.harga*td.qty) as total
                        FROM transaksi_detail as td,transaksi as t, user as o 
                        WHERE t.transaksi_id=td.transaksi_id and o.oprator_id=t.oprator_id group by t.transaksi_id";
        return $this->db->query($query);
    }
    
    function laporan_periode($tanggal1,$tanggal2)
    {
        $query= "SELECT t.transaksi_id, t.tanggal_transaksi,o.name,sum(td.harga*td.qty) as total 
                    FROM transaksi_detail as td,transaksi as t, user as o 
                    WHERE t.transaksi_id=td.transaksi_id and o.oprator_id=t.oprator_id and t.tanggal_transaksi between '$tanggal1' and '$tanggal2'
                    group by t.transaksi_id";
        return $this->db->query($query);
    }
    public function cetak2($id)
    {
//        $query = "SELECT * FROM barang b INNER JOIN transaksi_detail td, transaksi t where td.transaksi_id = t.transaksi_id";
        $query = "SELECT * FROM transaksi_detail td LEFT JOIN barang b ON td.barang_id = b.barang_id where td.transaksi_id = '".$id."' ";

        return $this->db->query($query);
        
    }

    public function cetak($id)
    {
//        $query = "SELECT * FROM barang b INNER JOIN transaksi_detail td, transaksi t where td.transaksi_id = t.transaksi_id";
        $query = "SELECT * FROM transaksi_detail td LEFT JOIN barang b ON td.barang_id = b.barang_id where td.transaksi_id = '".$id."' ";

        return $this->db->query($query);
        
    }


    function search_blog($title){
        $this->db->like('kode_barang', $title , 'both');
        $this->db->order_by('kode_barang', 'ASC');
        $this->db->limit(10);
        return $this->db->get('barang')->result();
    }

    function get_nis($nis){
        $hsl=$this->db->query("SELECT * FROM barang WHERE kode_barang='$nis'");
        if($hsl->num_rows()>0){
            foreach ($hsl->result() as $data) {
                $hasil=array(
                    'kode_barang' => $data->kode_barang,
                    'nama_barang' => $data->nama_barang,
                    'harga' => $data->harga,
                    'barang_id' => $data->barang_id,
                    );
            }
        }
        return $hasil;
    }
   

    function get_one($id)
    {
        $param = array('transaksi_id'=>$id);
        return $this->db->get_where('transaksi_detail',$param);
        
    }

    function detail($id){
       
        $query = "
        SELECT b.kode_barang, td.transaksi_id,td.qty,td.harga,b.nama_barang ,t.tanggal_transaksi ,sum(td.harga*td.qty) as total
        FROM transaksi_detail as td,barang as b , transaksi as t 
        WHERE b.barang_id=td.barang_id and td.transaksi_id = t.transaksi_id ORDER BY td.transaksi_id = '".$id."'
";
        return  $this->db->query($query);
    }


}